package messurements.sensor.praxis.distancecalculation;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Sarah on 22.06.2015.
 */
public class Orientation {

    float[] mValuesMagnet       = new float[3];
    float[] mValuesAccel        = new float[3];
    float[] mValuesOrientation  = new float[3];
    float[] mRotationMatrix     = new float[9];

    public Orientation(float[] mValuesMagnet, float[] mValuesAccel,
                       float[] mValuesOrientation, float[] mRotationMatrix){
        this.mValuesMagnet      = mValuesMagnet;
        this.mValuesAccel       = mValuesAccel;
        this.mValuesOrientation = mValuesOrientation;
        this.mRotationMatrix    = mRotationMatrix;
    }

    public double initOrientationMatrix(){

        boolean success = SensorManager.getRotationMatrix(mRotationMatrix, null, mValuesAccel, mValuesMagnet);

        SensorManager.getOrientation(mRotationMatrix, mValuesOrientation);

        // azimuth =  rotation around the Z axis.
        // pitch =  rotation around the X axis.
        // roll = rotation around the Y axis
        double azimuth = Math.toDegrees(mValuesOrientation[0]);
        double pitch = Math.toDegrees(mValuesOrientation[1]);
        double roll = Math.toDegrees(mValuesOrientation[2]);

        return roll;

    }

    // Register the event listener and sensor type.
    public static void setListners(SensorManager sensorManager, SensorEventListener mEventListener)
    {
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
    }
}
